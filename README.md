# String calculator

This is an implementation of "string calculator" coding kata. Requirements were taken from https://osherove.com/tdd-kata-1  
Some important implementation notes and assumptions below:

1. The program was implemented to fulfill only the given requirements and work for valid inputs. There is no validation of the input data unless it's stated  explicitely by the requirements (negative numbers).
2. The requirements state that input “1,\n” is not valid. I assume that it means that repetition of any delimiter more than once is not a valid scenario, e.g. input 1,,2 is also not valid
3. The requirements state that newline should be used as delimiter together with comma in the default scenario. I assume that when overriding the delimiter, newline doesn't work as delimiter anymore, so input like //#\n1#2\n3 is not valid
4. I assume that newlines, and square braces cannot be used as delimiters, so inputs like //\n\n1\n\2\n3, //[[]\n1[2[3 are not valid
5. I assume that numbers can be used as delimiters. The following input is then valid //0\n101 and will return 2. The requirements were not clear on this, it could return 101 as well.
6. Even though most of the test cases has the same structure and could be easily parameterized, I decided not to use parameterized tests in favor of descriptive test method names.
7. The application is not designed to handle large data volumes, utilize multithreading, nor is heavily low-level optimized.

## Usage
Run `test` gradle task to execute tests.
