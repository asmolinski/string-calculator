package org.asmolinski.stringcalculator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

class StringCalculatorTest {

  @Nested
  class BasicScenarios {

    @Test
    void shouldReturnZeroForEmptyInput() {
      // given
      final var input = "";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(0);
    }

    @Test
    void shouldReturnSingleNumber() {
      // given
      final var input = "1";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(1);
    }

    @Test
    void shouldSumMultipleNumbers() {
      // given
      final var input = "1,1,1,1,1";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(5);
    }

    @Test
    void shouldIgnoreNumbersLargerThan1000() {
      // given
      final var input = "1000,1001";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(1000);
    }

    @Test
    void shouldThrowOnNegativesAndIncludeThemInMessage() {
      // given
      final var input = "-2,1,-1";
      // when
      final Executable executable = () -> new StringCalculator().add(input);
      // then
      final NegativesNotAllowedException thrown = assertThrows(NegativesNotAllowedException.class, executable);
      assertThat(thrown)
        .hasMessageContaining("negatives not allowed")
        .hasMessageContaining("-2")
        .hasMessageContaining("-1");
    }
  }

  @Nested
  class Delimiters {

    @Test
    void shouldAllowNewlineAsDefaultDelimiter() {
      // given
      final var input = "1\n2,3";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(6);
    }

    @Test
    void shouldUseCustomSingleCharDelimiterNoBraces() {
      // given
      final var input = "//%\n1%2%3";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(6);
    }

    @Test
    void shouldUseCustomDelimiterSingleCharInBraces() {
      // given
      final var input = "//[%]\n1%2%3";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(6);
    }

    @Test
    void shouldUseCustomDelimiterMultipleCharsInBraces() {
      // given
      final var input = "//[%*-]\n1%*-2%*-3";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(6);
    }

    @Test
    void shouldUseMultipleCustomDelimitersWithVaryingLengths() {
      // given
      final var input = "//[*][**][***]\n1*1***1**1";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(4);
    }

    @Test
    void shouldReturnZeroOnEmptyInputWithDelimitersDefinition() {
      // given
      final var input = "//[*][**][***]\n";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(0);
    }

    // This is not clearly stated in requirements. It could return as well 101, or throw an exception.
    @Test
    void shouldUseNumberDelimiter() {
      // given
      final var input = "//0\n101";
      // when
      final int actual = new StringCalculator().add(input);
      // then
      assertThat(actual).isEqualTo(2);
    }
  }

}
