package org.asmolinski.stringcalculator;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class NumberParser {

  private static final Comparator<String> BY_LENGTH_DESCENDING = (s1, s2) -> s2.length() - s1.length();

  private final String numbers;
  private final String delimitersRegex;

  public NumberParser(String numbers, Set<String> delimiters) {
    this.numbers = numbers;
    this.delimitersRegex = createDelimitersRegex(delimiters);
  }

  Stream<Integer> splitToNumbers() {
    return Arrays.stream(numbers.split(delimitersRegex)).map(Integer::parseInt);
  }

  private String createDelimitersRegex(final Set<String> delimiters) {
    return delimiters.stream()
      // one delimiter could be prefix of another [*][**][**@], so we need to match the longer ones first
      .sorted(BY_LENGTH_DESCENDING)
      .map(Pattern::quote)
      .collect(Collectors.joining("|"));
  }

}
