package org.asmolinski.stringcalculator;

import java.util.stream.Stream;

public class StringCalculator {

  private static final int MAX_VALUE = 1000;
  
  public int add(final String rawInput) {
    final var input = new CalculatorInput(rawInput);
    if (input.isEmpty()) {
      return 0;
    }
    final Stream<Integer> numbers = input.getNumbersStream();
    return calculateSum(numbers);
  }

  private int calculateSum(Stream<Integer> numbers) {
    final var result = numbers.filter(i -> i <= MAX_VALUE)
      .map(ResultAccumulator::valueOf)
      .reduce(ResultAccumulator.EMPTY, ResultAccumulator::combineWith);
    if (result.containsNegatives()) {
      throw new NegativesNotAllowedException(result.getNegatives());
    }
    return result.getSum();
  }
}
