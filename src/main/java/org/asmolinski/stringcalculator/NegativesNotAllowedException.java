package org.asmolinski.stringcalculator;

import static java.lang.String.format;

import java.util.Set;

class NegativesNotAllowedException extends RuntimeException {

  public NegativesNotAllowedException(final Set<Integer> negatives) {
    super(format("negatives not allowed: %s", negatives));
  }

}
