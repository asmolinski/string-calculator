package org.asmolinski.stringcalculator;

import java.util.Set;
import java.util.stream.Stream;

class CalculatorInput {

  private static final String DELIMITER_LINE_PREFIX = "//";

  private final String delimitersPart;
  private final String numbersPart;

  CalculatorInput(final String rawInput) {
    if (rawInput.startsWith(DELIMITER_LINE_PREFIX)) {
      final String[] splitted = rawInput.split("\n", 2);
      delimitersPart = splitted[0].substring(DELIMITER_LINE_PREFIX.length());
      numbersPart = splitted[1];
    } else {
      delimitersPart = "";
      numbersPart = rawInput;
    }
  }

  boolean isEmpty() {
    return numbersPart.isEmpty();
  }

  Stream<Integer> getNumbersStream() {
    final Set<String> delimiters = new DelimiterParser(delimitersPart).parseDelimiters();
    return new NumberParser(numbersPart, delimiters).splitToNumbers();
  }

}
