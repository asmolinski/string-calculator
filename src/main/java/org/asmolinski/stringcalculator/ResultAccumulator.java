package org.asmolinski.stringcalculator;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toSet;

import java.util.Set;
import java.util.stream.Stream;

class ResultAccumulator {

  static final ResultAccumulator EMPTY = new ResultAccumulator(0, emptySet());

  private final int sum;
  private final Set<Integer> negatives;

  private ResultAccumulator(int sum, Set<Integer> negatives) {
    this.sum = sum;
    this.negatives = negatives;
  }

  static ResultAccumulator valueOf(final int value) {
    return new ResultAccumulator(value, value < 0 ? Set.of(value) : emptySet());
  }

  ResultAccumulator combineWith(ResultAccumulator other) {
    final var unionOfNegatives = Stream.concat(negatives.stream(), other.negatives.stream()).collect(toSet());
    return new ResultAccumulator(sum + other.sum, unionOfNegatives);
  }

  int getSum() {
    return sum;
  }

  Set<Integer> getNegatives() {
    return negatives;
  }

  boolean containsNegatives() {
    return !negatives.isEmpty();
  }
}
