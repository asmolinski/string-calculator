package org.asmolinski.stringcalculator;

import static java.util.Collections.unmodifiableSet;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

class DelimiterParser {

  private static final Set<String> DEFAULT_DELIMITERS = Set.of(",", "\n");
  private static final Pattern DELIMITER_IN_BRACES_PATTERN = Pattern.compile("\\[(.*?)\\]");
  private final String delimiters;

  DelimiterParser(String delimiters) {
    this.delimiters = delimiters;
  }

  Set<String> parseDelimiters() {
    return hasCustomDelimiters()
      ? getDelimitersFromInput()
      : DEFAULT_DELIMITERS;
  }

  private boolean hasCustomDelimiters() {
    return !delimiters.isEmpty();
  }

  private Set<String> getDelimitersFromInput() {
    return hasDelimitersInBracedFormat()
      ? extractBracedDelimiters(delimiters)
      : Set.of(delimiters);
  }

  private boolean hasDelimitersInBracedFormat() {
    return delimiters.contains("[");
  }

  private Set<String> extractBracedDelimiters(String delimitersPart) {
    final var matcher = DELIMITER_IN_BRACES_PATTERN.matcher(delimitersPart);
    final var result = new HashSet<String>();
    while (matcher.find()) {
      result.add(matcher.group(1));
    }
    return unmodifiableSet(result);
  }
}
